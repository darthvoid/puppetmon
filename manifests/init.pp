#
# Monitor Puppet Last run state
#
########################################################################
#
# === Parameters
#
# [*python_yaml_package*]
#   Name of the package containing PyYAML library
#
# === Author
# - Original version: Yvan Broccard <yvan.broccard@hopitalvs.ch>
#
class puppetmon (
  $python_yaml_package = $puppetmon::params::python_yaml_package
)
  inherits puppetmon::params
{

  # read lastrunfile from configuration parameters ( = puppet config print)
  # unfor it's read on the master, not the agent
  # $lastrunfile = $settings::lastrunfile
  case $::puppetversion {
    /^3\./: {
      $lastrunfile = '/var/lib/puppet/state/last_run_summary.yaml'
      $cachedir = '/var/lib/puppet'
    }
    default,/^4\./: {
      $lastrunfile = '/opt/puppetlabs/puppet/cache/state/last_run_summary.yaml'
      $cachedir = '/opt/puppetlabs/puppet/cache'
    }
  }

  if ! defined(Package[$python_yaml_package]) {
    package { $python_yaml_package:
      ensure => installed,
    }
  }

  # ensure zabbix user can read last_run_summary.yaml
  file { $cachedir:
    ensure => directory,
    mode   => '0755',
  }

  file {'/usr/local/sbin/parsepuplastrun':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    seltype => 'zabbix_agent_exec_t',
    content => template("${module_name}/parsepuplastrun.erb"),
  }

  file {'/etc/zabbix/zabbix_agentd.d/userparameter_puppetmon.conf':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => "puppet:///modules/${module_name}/userparameter_puppetmon.conf",
    notify => Service['zabbix-agent'],
  }

  # selinux : allow zabbix_agent_t puppet_var_lib_t:file read;
  if str2bool($::selinux_enforced) {
    selinux::semodule {'puppetmon_local':
      source => "puppet:///modules/${module_name}/puppetmon_local.te",
    }
  }
}
