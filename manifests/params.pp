# === Parameters
#
# No parameters for param class
#
# === Author
# - Original version: Yvan Broccard <yvan.broccard@hopitalvs.ch>
#
class hvs_zabbix_checks::params {

  case $::osfamily {
    'Debian': {
      $python_yaml_package = 'python-yaml'
    }
    'RedHat': {
      case $facts['os']['distro']['release']['major'] {
        '8': {
          $python_yaml_package = 'python2-pyyaml'
        }
        '5','6','7',default: {
          $python_yaml_package = 'PyYAML'
        }
        '32','33': {
          # For Fedora
          $python_yaml_package = 'python3-pyyaml'
        }
      }
    }
    default: {
      fail("Unsupported osfamily (${::osfamily})")
    }
  }
}
